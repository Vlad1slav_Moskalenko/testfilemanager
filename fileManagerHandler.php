<?
include_once 'FileManager.php';

$fm = new FileManager();
$fm->currentPath = $_POST['path'];

if(is_file($_POST['path'])) {
    foreach ($fm->getFileCotent($_POST['path']) as $value) {
        echo htmlentities($value) . '</br>';
    }
    exit;
}

if ($_POST['back']) {
    $fm->currentPath = $_POST['back'];
    echo 'Current path: \''.$fm->currentPath.'\'</br></br>';
    echo $fm->back($_POST['currentPath']);
    exit;
}
else {
    //$fm->currentPath = $_POST['path'];
    echo 'Current path: \''.$fm->currentPath.'\'</br></br>';
    echo $fm->generateHtml($_POST['path']);
}
