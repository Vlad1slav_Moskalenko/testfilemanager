<?php
/**
 * Created by PhpStorm.
 * User: Moskalenko
 * Date: 23.10.2017
 * Time: 15:30
 */

class FileManager
{
    public $currentPath = '.';

    public function getContent($path)
    {
        return scandir($path, 1);
    }

    public function generateHtml ($path = '.')
    {
        $html = '';
        $dirContent = $this->getContent($path);

        $html .= "<form action='fileManagerHandler.php' method='post'>
                    <input type='hidden' name='currentPath' value='$this->currentPath'>
                    <button name='back' value='" . $this->backPath($this->currentPath) . "'>..</button></br>";

        foreach ($dirContent as $value) {
            if ($value === '.' || $value === '..')
                continue;
            $html .= ("<button name='path' value='$this->currentPath/$value' title='$value'>" . $value . '</button></br>');
        }

        $html .= '</form>';

        return $html;

    }

    public function back($path)
    {
        if($path === '.')
            return $this->generateHtml('.');
        preg_match('/(\S*)\/\S*/', $path, $matches);
        $this->currentPath = $matches[1];
        return $this->generateHtml($matches[1]);
    }

    public function backPath($path)
    {
        if ($this->currentPath === '.')
            return '.';
        preg_match('/(\S*)\/\S*/', $path, $matches);
        return $matches[1];
    }

    public function getFileCotent($path)
    {
        return file($path);
    }

}